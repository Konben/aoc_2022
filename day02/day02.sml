exception InvalidInput;


datatype handShape  = Rock | Paper | Scissors;
datatype outcome    = Win  | Draw  | Lose;
type     tournament = (handShape * outcome) list;


fun parseLine l =
    let
        fun rem2nd (x::(_::xs)) = x :: xs
        |   rem2nd _            = raise InvalidInput;

        fun dropLast nil      = nil
        |   dropLast (_::nil) = nil
        |   dropLast (x::xs)  = x :: dropLast xs;

        fun decryptShape #"A" = Rock
        |   decryptShape #"B" = Paper
        |   decryptShape #"C" = Scissors
        |   decryptShape _    = raise InvalidInput;

        fun decryptOutcome #"X" = Lose
        |   decryptOutcome #"Y" = Draw
        |   decryptOutcome #"Z" = Win
        |   decryptOutcome _    = raise InvalidInput;
    in
        case rem2nd (dropLast (explode l)) of
            [shape, out] => (decryptShape shape, decryptOutcome out)
        |   _            => raise InvalidInput
    end;

fun parseInput ls = List.foldr (fn (l, rs) => parseLine l :: rs) nil ls;

fun scoreRound shape out =
    let
        fun shapeScore Rock     = 1
        |   shapeScore Paper    = 2
        |   shapeScore Scissors = 3;

        fun outcomeScore Lose = 0
        |   outcomeScore Draw = 3
        |   outcomeScore Win  = 6;

        fun choosePlay Rock Lose     = Scissors
        |   choosePlay Rock Draw     = Rock
        |   choosePlay Rock Win      = Paper
        |   choosePlay Paper Lose    = Rock
        |   choosePlay Paper Draw    = Paper
        |   choosePlay Paper Win     = Scissors
        |   choosePlay Scissors Lose = Paper
        |   choosePlay Scissors Draw = Scissors
        |   choosePlay Scissors Win  = Rock;
    in
        shapeScore (choosePlay shape out) + outcomeScore out
    end;

fun scoreTournament rounds = List.foldr (fn ((shape, out), s) => scoreRound shape out + s) 0 rounds;
