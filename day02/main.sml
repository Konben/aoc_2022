open TextIO;
open Int;

fun readFile inFile =
    let
        fun worker NONE     = nil
        |   worker (SOME l) = l :: worker (inputLine inFile);
    in
        worker (inputLine inFile)
    end;

val inFile = openIn "input";
val t = parseInput (readFile inFile);
val score = scoreTournament t;

print (toString score ^ "\n");
