package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	// Open file
	inFile, err := os.Open("input")
	if err != nil {
		fmt.Fprint(os.Stderr, err.Error())
		os.Exit(1)
	}
	defer inFile.Close()

	// Read & parse input line by line
	sacks := []rucksack{}
	scanner := bufio.NewScanner(inFile)
	for scanner.Scan() {
		sacks = append(sacks, rucksack(scanner.Text()))
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprint(os.Stderr, err.Error())
		os.Exit(1)
	}

	// Solve p1
	sum := prioritySum(sacks)
	fmt.Printf("Solution to part 1: %d\n", sum)

	// Solve p2
	sum = groupSum(sacks)
	fmt.Printf("Solution to part 2: %d\n", sum)
}

type itemType byte
type rucksack []itemType

// Returns the first & second compartment of the rucksack as sets.
func (r rucksack) compartments() (map[itemType]bool, map[itemType]bool) {
	firstComp, secondComp := r[:len(r)/2], r[len(r)/2:]
	return toSet(firstComp), toSet(secondComp)
}

// Converst the given slice into a set.
func toSet(itemSlice []itemType) map[itemType]bool {
	set := map[itemType]bool{}
	for _, item := range itemSlice {
		set[item] = true
	}
	return set
}

// Returnst the priority of an item type.
func priority(item itemType) int {
	if item >= 'a' && item <= 'z' {
		return int(item - 'a' + 1)
	}
	return int(item - 'A' + 27)
}

// Computes the solution to part 1 of the puzzle.
func prioritySum(sacks []rucksack) int {
	sum := 0
	for _, s := range sacks {
		firstComp, secondComp := s.compartments()
		for item := range firstComp {
			if secondComp[item] {
				sum += priority(item)
				break
			}
		}
	}
	return sum
}

// Computes the solution to part 2 of the puzzle.
func groupSum(sacks []rucksack) int {
	sum := 0
	for i := 0; i <= len(sacks)-3; i += 3 {
		s1, s2, s3 := sacks[i], toSet(sacks[i+1]), toSet(sacks[i+2])
		for _, item := range s1 {
			if s2[item] && s3[item] {
				sum += priority(item)
				break
			}
		}
	}
	return sum
}
