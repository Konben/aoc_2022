exception InvalidInput;


(* Splits the given list into two halves. *)
fun split xs =
    let
        val len = length xs;
    in
        (List.take (xs, (len div 2)), List.drop (xs, (len div 2)))
    end;

(* Checks if the list contains a given element. *)
fun doesContain nil _     = false
|   doesContain (x::xs) a = (x = a) orelse doesContain xs a;

(* Finds the common item type in both compartments of a rucksack. *)
fun commonType nil _              = raise InvalidInput
|   commonType (t::ts) secondComp = if doesContain secondComp t then t
                                    else commonType ts secondComp;

(* Computes the priority of the given item type. *)
fun priority t = if Char.isLower t then ord t - ord #"a" + 1
                 else ord t - ord #"A" + 27;

(* Computes the puzzle solution given a list of rucksacks (as a list of strings). *)
fun prioritySum nil     = 0
|   prioritySum (r::rs) =
        let
            val compartments = split (explode r);
        in
            priority (commonType (#1 compartments) (#2 compartments)) + prioritySum rs
        end;

(* Computes the puzzle solution for p2. *)
fun groupSum nil = 0
|   groupSum (r1::(r2::(r3::rs))) =
        let
            fun findMatching nil _ _       = raise InvalidInput
            |   findMatching (x::xs) ys zs = if doesContain ys x andalso doesContain zs x then x
                                             else findMatching xs ys zs;
        in
            priority (findMatching (explode r1) (explode r2) (explode r3)) + groupSum rs
        end
|   groupSum _ = raise InvalidInput;
