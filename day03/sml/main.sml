open TextIO;
open Char;
open Int;

(* Reads inFile line by line and returns the lines as a string list (without NLs). *)
fun readFile inFile =
    let
        fun remNL nil          = nil
        |   remNL (#"\n"::nil) = nil
        |   remNL (c::cs)      = c :: remNL cs;

        fun worker NONE = []
        |   worker (SOME l) = implode (remNL (explode l)) :: worker (inputLine inFile);
    in
        worker (inputLine inFile)
    end;

(* Reading puzzle input *)
val inFile = openIn "input";
val input = readFile inFile;

print (toString (prioritySum input) ^ "\n");
print (toString (groupSum input) ^ "\n");
