package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	// Open file
	inFile, err := os.Open("input")
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
	defer inFile.Close()

	// Reading & parsing drawing
	scanner := bufio.NewScanner(inFile)
	drawing := []string{}
	for scanner.Scan() {
		if scanner.Text() != "" {
			drawing = append(drawing, scanner.Text())
		} else {
			break
		}
	}
	supplyStacks := parseStackDrawing(drawing)

	// Reading & parsing instructions
	for scanner.Scan() {
		inst := parseInstruction(scanner.Text())
		// Evaluate instruction
		inst.eval(supplyStacks)
	}

	// Print top crates
	for _, s := range supplyStacks {
		top, _ := s.pop()
		fmt.Printf("%c", top)
	}
	fmt.Println()
}

type stack []byte

func (s *stack) push(c byte) {
	*s = append(*s, c)
}

func (s *stack) pop() (byte, error) {
	if len(*s) == 0 {
		return 0, errors.New("pop: Cannot pop an empty stack")
	}
	top := (*s)[len(*s)-1]
	*s = (*s)[:len(*s)-1]
	return top, nil
}

// Boy this function is a mess...
func parseStackDrawing(drawing []string) []stack {
	// Find the number of supply stacks
	base := drawing[len(drawing)-1]
	noOfStacks := len(strings.Split(base, "   "))

	// Remove base
	drawing = drawing[:len(drawing)-1]

	// Parse stacks
	supplyStacks := make([]stack, noOfStacks)
	for i := range drawing {
		i = len(drawing) - i - 1
		crates := drawing[i]
		for j := 0; j < noOfStacks; j++ {
			// Read crate
			c := crates[:3]
			if c != "   " {
				supplyStacks[j].push(c[1])
			}
			if len(crates) >= 4 {
				crates = crates[4:]
			}
		}
	}

	return supplyStacks
}

type instruction struct {
	move, from, to uint
}

func parseInstruction(inst string) instruction {
	split := strings.Split(inst, " ")
	move, _ := strconv.ParseUint(split[1], 10, 0)
	from, _ := strconv.ParseUint(split[3], 10, 0)
	to, _ := strconv.ParseUint(split[5], 10, 0)
	return instruction{uint(move), uint(from), uint(to)}
}

func (i instruction) eval(supplyStacks []stack) {
	crates := []byte{}
	for k := uint(0); k < i.move; k++ {
		topCrate, _ := supplyStacks[i.from-1].pop()
		crates = append(crates, topCrate)
	}
	for k := range crates {
		k = len(crates) - k - 1
		crate := crates[k]
		supplyStacks[i.to-1].push(crate)
	}
}
