package main

import (
	"fmt"
	"os"
)

func main() {
	b, err := os.ReadFile("input")
	if err != nil {
		panic(err)
	}

	fmt.Println("first start-of-message marker at", findMarker(b))
}

const markerLength = 14

func findMarker(msg []byte) int {
	for i := markerLength - 1; i < len(msg); i++ {
		charSet := map[byte]bool{}

		for j := 0; j < markerLength; j++ {
			charSet[msg[i-j]] = true
		}

		if len(charSet) == markerLength {
			return i + 1
		}
	}

	return -1
}
