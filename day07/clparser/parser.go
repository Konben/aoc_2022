package clparser

import (
	"bufio"
	"errors"
	"strconv"
)

type File struct {
	Size uint
}

type Directory struct {
	Files          map[string]File
	Subdirectories map[string]*Directory
	Parent         *Directory
}

var Reader *bufio.Reader = nil

// Reads the next token from inFile and returns true if the current
// lookahead is a t token, and returns false otherwise.
func accept(t tokenType) bool {
	if lookahead.token == t {
		// I'm just assuming that no error is gonna
		// be occuring here, professional as always.
		readToken(Reader)
		return true
	}
	return false
}

// Parses the puzzle input and returns the root of the filesystem.
func ParsePuzzleInput() (*Directory, error) {
	// Read initial token
	err := readToken(Reader)
	if err != nil {
		return &Directory{}, err
	}

	// Parse filesystem
	root := &Directory{
		Parent:         nil,
		Subdirectories: map[string]*Directory{},
		Files:          map[string]File{},
	}
	wd := root
	for {
		// Read $
		if !accept(DOLLAR) {
			return root, errors.New("expected a '$' before every command")
		}

		if accept(CD) {
			// cd command
			name := lookahead.lexeme
			if !accept(NAME) {
				return root, errors.New("expected a name after cd")
			}
			if name == "/" {
				wd = root
			} else if name == ".." {
				wd = wd.Parent
			} else {
				_, found := wd.Subdirectories[name]
				if !found {
					wd.Subdirectories[name] = &Directory{
						Parent:         wd,
						Subdirectories: map[string]*Directory{},
						Files:          map[string]File{},
					}
				}
				wd = wd.Subdirectories[name]
			}

			// Read newline or eof
			if accept(EOF) {
				break
			} else if !accept(NL) {
				return root, errors.New("expected either EOF or a NL after your command")
			}
		} else if accept(LS) {
			// ls command
			// Read newline or eof
			if accept(EOF) {
				break
			} else if !accept(NL) {
				return root, errors.New("expected either EOF or a NL after your command")
			}

			// Parse ls output
			err = parseLsOutput(wd)
			if err != nil {
				return root, err
			}
		} else {
			return root, errors.New("expected a valid command after the '$'")
		}

		// at eof?
		if lookahead.token == EOF {
			break
		}
	}

	return root, nil
}

// Parses the output of the ls command.
func parseLsOutput(wd *Directory) error {
	for lookahead.token != DOLLAR {
		if accept(DIR) {
			name := lookahead.lexeme
			if !accept(NAME) {
				return errors.New("expected a name after dir")
			}
			_, found := wd.Subdirectories[name]
			if !found {
				wd.Subdirectories[name] = &Directory{
					Parent:         wd,
					Subdirectories: map[string]*Directory{},
					Files:          map[string]File{},
				}
			}
		} else {
			size := lookahead.lexeme
			if !accept(FILE_SIZE) {
				return errors.New("expected a file size")
			}
			name := lookahead.lexeme
			if !accept(NAME) {
				return errors.New("expected a name after the file size")
			}
			_, found := wd.Subdirectories[name]
			if !found {
				s, _ := strconv.ParseUint(size, 10, 0)
				wd.Files[name] = File{uint(s)}
			}
		}

		// Read newline or eof
		if accept(EOF) {
			break
		} else if !accept(NL) {
			return errors.New("expected either EOF or a NL after ls output")
		}
	}
	return nil
}
