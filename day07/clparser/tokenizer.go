package clparser

import (
	"bufio"
	"io"
)

// Tokens
type tokenType int

const (
	DOLLAR    tokenType = iota
	NAME                = iota
	CD                  = iota
	LS                  = iota
	DIR                 = iota
	FILE_SIZE           = iota
	NL                  = iota
	EOF                 = iota
)

// Lookahead symbol
var lookahead = struct {
	token  tokenType
	lexeme string
}{}

var keywords = map[string]tokenType{
	"cd":  CD,
	"ls":  LS,
	"dir": DIR,
}

// Reads a token from reader.
func readToken(reader *bufio.Reader) error {
	// Ignore spaces
	c := byte(' ')
	var err error = nil
	for isSpace(c) && err == nil {
		c, err = reader.ReadByte()
	}

	// Handle EOF
	if err != nil {
		if err != io.EOF {
			return err
		}
		lookahead.token = EOF
		lookahead.lexeme = ""
		return nil
	}

	// Newlines
	if c == '\n' {
		lookahead.token = NL
		lookahead.lexeme = "\n"
		return nil
	}

	// $ sign
	if c == '$' {
		lookahead.token = DOLLAR
		lookahead.lexeme = "$"
		return nil
	}

	// File sizes
	if isDigit(c) && err == nil {
		size := ""
		for isDigit(c) {
			size += string(c)
			c, err = reader.ReadByte()
		}
		if err == nil {
			reader.UnreadByte()
		}
		lookahead.token = FILE_SIZE
		lookahead.lexeme = size
		return nil
	}

	// Names & keywords
	name := ""
	for !isSpace(c) && c != '\n' && err == nil {
		name += string(c)
		c, err = reader.ReadByte()
		if err != nil {
			break
		}
	}
	if err == nil {
		reader.UnreadByte()
	}
	if token, in := keywords[name]; in {
		lookahead.token = token
	} else {
		lookahead.token = NAME
	}
	lookahead.lexeme = name
	return nil
}

func isSpace(c byte) bool {
	return c == ' ' || c == '\t'
}

func isDigit(c byte) bool {
	return c >= '0' && c <= '9'
}
