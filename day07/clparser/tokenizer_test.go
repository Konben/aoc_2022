package clparser

import (
	"bufio"
	"os"
	"testing"
)

func TestReadToken(t *testing.T) {
	// Open file
	inFile, err := os.Open("test_input")
	if err != nil {
		t.Log("Error not nil: " + err.Error())
		t.Fail()
	}
	defer inFile.Close()
	reader := bufio.NewReader(inFile)

	// Expected tokens
	expected := []struct {
		token  tokenType
		lexeme string
	}{
		{DOLLAR, "$"}, // cd /
		{CD, "cd"},
		{NAME, "/"},
		{NL, "\n"},
		{DOLLAR, "$"}, // ls
		{LS, "ls"},
		{NL, "\n"},
		{DIR, "dir"}, // dir a
		{NAME, "a"},
		{NL, "\n"},
		{FILE_SIZE, "14848514"}, // 14848514 b.txt
		{NAME, "b.txt"},
		{NL, "\n"},
		{FILE_SIZE, "8504156"}, // 8504156 c.dat
		{NAME, "c.dat"},
		{NL, "\n"},
		{DIR, "dir"}, // dir d
		{NAME, "d"},
		{NL, "\n"},
		{DOLLAR, "$"}, // cd a
		{CD, "cd"},
		{NAME, "a"},
		{NL, "\n"},
		{DOLLAR, "$"}, // ls
		{LS, "ls"},
		{NL, "\n"},
		{DIR, "dir"}, // dir e
		{NAME, "e"},
		{NL, "\n"},
		{FILE_SIZE, "29116"}, // 29116 f
		{NAME, "f"},
		{NL, "\n"},
		{FILE_SIZE, "2557"}, // 2557 g
		{NAME, "g"},
		{NL, "\n"},
		{FILE_SIZE, "62596"}, // 62596 h.lst
		{NAME, "h.lst"},
		{NL, "\n"},
		{DOLLAR, "$"}, // cd e
		{CD, "cd"},
		{NAME, "e"},
		{NL, "\n"},
		{DOLLAR, "$"}, // ls
		{LS, "ls"},
		{NL, "\n"},
		{FILE_SIZE, "584"}, // 584 i
		{NAME, "i"},
		{NL, "\n"},
		{DOLLAR, "$"}, // cd ..
		{CD, "cd"},
		{NAME, ".."},
		{NL, "\n"},
		{DOLLAR, "$"}, // cd ..
		{CD, "cd"},
		{NAME, ".."},
		{NL, "\n"},
		{DOLLAR, "$"}, // cd d
		{CD, "cd"},
		{NAME, "d"},
		{NL, "\n"},
		{DOLLAR, "$"}, // ls
		{LS, "ls"},
		{NL, "\n"},
		{FILE_SIZE, "4060174"}, // 4060174 j
		{NAME, "j"},
		{NL, "\n"},
		{FILE_SIZE, "8033020"}, // 8033020 d.log
		{NAME, "d.log"},
		{NL, "\n"},
		{FILE_SIZE, "5626152"}, // 5626152 d.ext
		{NAME, "d.ext"},
		{NL, "\n"},
		{FILE_SIZE, "7214296"}, // 7214296 k
		{NAME, "k"},
		{EOF, ""},
	}

	// Test
	for _, e := range expected {
		err = readToken(reader)
		if err != nil {
			t.Log("Error not nil: " + err.Error())
			t.Fail()
		}
		if e.token != lookahead.token || e.lexeme != lookahead.lexeme {
			t.Log("Expected:", e.token, e.lexeme, "but got", lookahead.token, lookahead.lexeme)
			t.Fail()
		}
	}
}
