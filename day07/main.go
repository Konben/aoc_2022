package main

import (
	"bufio"
	"day07/clparser"
	"fmt"
	"os"
)

func main() {
	// Parse the puzzle input
	inFile, err := os.Open("input")
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
	defer inFile.Close()
	clparser.Reader = bufio.NewReader(inFile)
	root, err := clparser.ParsePuzzleInput()
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	// Find all directories with size <= 100000
	dirSizes := computeDirSizes(root, "/")
	dirSum := uint(0)
	for _, s := range dirSizes {
		if s <= 100000 {
			dirSum += s
		}
	}
	fmt.Printf("The sum of dirs with size <= 100000 is %d.\n", dirSum)

	// Find the smallest file that, if deleted,
	// frees up enough space for the update.
	const totalSpace uint = 70000000
	const spaceNeeded uint = 30000000
	freeSpace := totalSpace - dirSizes["/"]
	smallestDir, size := "/", dirSizes["/"]
	for path, s := range dirSizes {
		if freeSpace+s >= spaceNeeded && s < size {
			smallestDir = path
			size = s
		}
	}
	fmt.Printf("You should delete %s of size %d.\n", smallestDir, size)
}

// Prints the filesystem to stdout.
// Used for debugging
/*
func printFilesystem(root *clparser.Directory, indent int) {
	for name, file := range root.Files {
		fmt.Print(strings.Repeat("\t", indent))
		fmt.Println("-", name, "(file, size="+strconv.Itoa(int(file.Size))+")")
	}
	for name, dir := range root.Subdirectories {
		fmt.Print(strings.Repeat("\t", indent))
		fmt.Println("-", name, "(dir)")
		printFilesystem(dir, indent+1)
	}
}
*/

// Computes the sizes of all directories in the filesystem.
func computeDirSizes(root *clparser.Directory, prefix string) map[string]uint {
	totalSize := uint(0)
	for _, file := range root.Files {
		totalSize += file.Size
	}
	sizes := map[string]uint{}
	for name, dir := range root.Subdirectories {
		newPrefix := prefix + name + "/"
		subSizes := computeDirSizes(dir, newPrefix)

		// Not the fastest way to do things,
		// but it works for now.
		for sub, s := range subSizes {
			sizes[sub] = s
		}
		totalSize += subSizes[newPrefix]
	}
	sizes[prefix] = totalSize
	return sizes
}
