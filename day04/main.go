package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	// Open file
	inFile, err := os.Open("input")
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
	defer inFile.Close()

	// Reading puzzle input
	scanner := bufio.NewScanner(inFile)
	containedCount, overlappingCount := 0, 0
	for scanner.Scan() {
		// Split line
		line := scanner.Text()
		ranges := strings.Split(line, ",")
		if len(ranges) != 2 {
			fmt.Fprintln(os.Stderr, "Invalid number of ranges in line!")
			os.Exit(1)
		}

		// Parse ranges
		r1, err := parseIdRange(ranges[0])
		if err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(1)
		}
		r2, err := parseIdRange(ranges[1])
		if err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(1)
		}

		// Check containment
		if r1.fullyContains(r2) || r2.fullyContains(r1) {
			containedCount++
		}

		// Check overlap
		if r1.overlapsWith(r2) {
			overlappingCount++
		}

		// Faster way to do it
		/*
			if r1.fullyContains(r2) || r2.fullyContains(r1) {
				containedCount++
				overlappingCount++
			} else if r1.contains(r2.min) || r1.contains(r2.max) {
				overlappingCount++
			}
		*/
	}

	// Ouput
	fmt.Printf("There are %d pairs where one range fully contains the other.\n", containedCount)
	fmt.Printf("There are %d overlapping pairs.\n", overlappingCount)
}

type idRange struct {
	min, max uint
}

// Returns true iff x is in r.
func (r idRange) contains(x uint) bool {
	return r.min <= x && r.max >= x
}

// Returns true iff other is fully contained in r.
func (r idRange) fullyContains(other idRange) bool {
	return r.min <= other.min && r.max >= other.max
}

// Returns true iff other overlaps with r.
func (r idRange) overlapsWith(other idRange) bool {
	return r.contains(other.min) ||
		r.contains(other.max) ||
		other.fullyContains(r)
}

// Parses a string "a-b" into the range idRange{a, b}.
func parseIdRange(r string) (idRange, error) {
	split := strings.Split(r, "-")
	if len(split) != 2 {
		return idRange{}, errors.New("parseRange:" + " Too many/few items separated by a '-' in \"" + r + "\"!")
	}
	min, err := strconv.ParseUint(split[0], 10, 0)
	if err != nil {
		return idRange{}, err
	}
	max, err := strconv.ParseUint(split[1], 10, 0)
	if err != nil {
		return idRange{}, err
	}
	return idRange{uint(min), uint(max)}, nil
}
